

import matplotlib.pyplot as plt
import torch 
import torch.nn as nn
import torchvision
import torchvision.datasets as dsets
import torchvision.transforms as transforms
from torch.autograd import Variable
from sklearn.metrics import confusion_matrix
from datetime import timedelta
import numpy as np
import time

transform = transforms.Compose(
    [transforms.ToTensor()])
#======================== MNIST Dataset==================================================================
train_dataset = dsets.CIFAR10(root='./data/',
                            train=True, 
                            transform=transform,
                            download=True)

test_dataset = dsets.CIFAR10(root='./data/',
                           train=False, 
                           transform=transform)

class_names = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
# Parameters
batch_size = 8
acc_batch_size = 8
learning_rate = 0.0001
img_size = 32
img_shape = (img_size, img_size) #image shape
num_classes = 10
all_categories = []
cuda_available = cuda = torch.cuda.is_available()

for x in range(0,10):
    all_categories.append(class_names[x])
# Data Loader (Input Pipeline)
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size, 
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=acc_batch_size,
                                          shuffle=False)


test_loaderAll = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=len(test_dataset),
                                          shuffle=False)


#=====================================================Plot=================================================#
def plot_images_numpy(images,cls_true,cls_pred=None): # vykresleni obrazku,konvertace na numpy pole
    out = torchvision.utils.make_grid(images)
    out = images.numpy()
    plot_images(out,cls_true,cls_pred)

def plot_images(images, cls_true, cls_pred=None, smooth=True): # vykresleni obrazku
    assert len(images) == len(cls_true) == 9 # v jednom plotu bude 9 obrazku
    fig, axes = plt.subplots(3, 3) # matice 3x3

    if cls_pred is None:
        hspace = 0.3 # mezera mezi subploty
    else:
        hspace = 0.6
    fig.subplots_adjust(hspace=hspace, wspace=0.3) # nastaveni mezer sublotu

    for i, ax in enumerate(axes.flat):
        if smooth: # pouziti smooth
            interpolation = 'spline16'
        else:
            interpolation = 'nearest'

    
        ax.imshow(np.transpose(images[i], (1, 2, 0)),interpolation=interpolation) # vykresleni
        numberClass = cls_true[i] # vypsani do jake tridy patri dle predikce
        cls_true_name = class_names[numberClass] # do jake tridy patri dle datasetu

        if cls_pred is None: # vypisuji bez predikce
            xlabel = "True: {0}".format(cls_true_name)
        else: # s predikci
            cls_pred_name = class_names[int(cls_pred[i])]
            xlabel = "True: {0}\nPred: {1}".format(cls_true_name, cls_pred_name)

        ax.set_xlabel(xlabel) # nastaveni popisku      
        ax.set_xticks([]) # souradnice x nebudou
        ax.set_yticks([]) # souradnice y nebudou

    plt.show()

def plot_image_numpy(image): # vykresleni jednoho obrazku a konvertace na numpy
    out = torchvision.utils.make_grid(image)
    out = image.numpy()
    plot_image(out)

def plot_image(image): # vykresleni jednoho konkretniho obrazku
    
    fig, axes = plt.subplots(1, 2)

    ax0 = axes.flat[0]
    ax1 = axes.flat[1]

    ax0.imshow(image, interpolation='nearest')
    ax1.imshow(image, interpolation='spline16')

    ax0.set_xlabel('Raw')
    ax1.set_xlabel('Smooth')
    
    plt.show()

def Dataset_Info(): # info o datasetu
    print("Size of:")
    print("- Training-set:\t\t{}".format(len(train_dataset)))
    print("- Test-set:\t\t{}".format(len(test_dataset)))
    
# GoogLeNet Model =============================================================================

class Inception(nn.Module): # model
    def __init__(self, in_planes, n1x1, n3x3red, n3x3, n5x5red, n5x5, pool_planes):
        super(Inception, self).__init__()
        # 1x1 
        self.b1 = nn.Sequential(
            nn.Conv2d(in_planes, n1x1, kernel_size=1),
            nn.BatchNorm2d(n1x1),
            nn.ReLU(True),
        )
        # 1x1 -> 3x3 
        self.b2 = nn.Sequential(
            nn.Conv2d(in_planes, n3x3red, kernel_size=1),
            nn.BatchNorm2d(n3x3red),
            nn.ReLU(True),
            nn.Conv2d(n3x3red, n3x3, kernel_size=3, padding=1),
            nn.BatchNorm2d(n3x3),
            nn.ReLU(True),
        )
        # 1x1  -> 5x5 
        self.b3 = nn.Sequential(
            nn.Conv2d(in_planes, n5x5red, kernel_size=1),
            nn.BatchNorm2d(n5x5red),
            nn.ReLU(True),
            nn.Conv2d(n5x5red, n5x5, kernel_size=3, padding=1),
            nn.BatchNorm2d(n5x5),
            nn.ReLU(True),
            nn.Conv2d(n5x5, n5x5, kernel_size=3, padding=1),
            nn.BatchNorm2d(n5x5),
            nn.ReLU(True),
        )
        # 3x3 pool -> 1x1 conv 
        self.b4 = nn.Sequential(
            nn.MaxPool2d(3, stride=1, padding=1),
            nn.Conv2d(in_planes, pool_planes, kernel_size=1),
            nn.BatchNorm2d(pool_planes),
            nn.ReLU(True),
        )

    def forward(self, x):
        y1 = self.b1(x)
        y2 = self.b2(x)
        y3 = self.b3(x)
        y4 = self.b4(x)
        return torch.cat([y1,y2,y3,y4], 1)

class GoogLeNet(nn.Module):
    def __init__(self):
        super(GoogLeNet, self).__init__()
        self.pre_layers = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=7, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(True),
            nn.Conv2d(64, 64, kernel_size=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(True),
            nn.Conv2d(64,192, kernel_size=3, padding=1),
            nn.BatchNorm2d(192),
            nn.ReLU(True),
        )

        self.a3 = Inception(192,  64,  96, 128, 16, 32, 32)
        self.b3 = Inception(256, 128, 128, 192, 32, 96, 64)

        self.maxpool = nn.MaxPool2d(3, stride=2, padding=1)

        self.a4 = Inception(480, 192,  96, 208, 16,  48,  64)
        self.b4 = Inception(512, 160, 112, 224, 24,  64,  64)
        self.c4 = Inception(512, 128, 128, 256, 24,  64,  64)
        self.d4 = Inception(512, 112, 144, 288, 32,  64,  64)
        self.e4 = Inception(528, 256, 160, 320, 32, 128, 128)

        self.a5 = Inception(832, 256, 160, 320, 32, 128, 128)
        self.b5 = Inception(832, 384, 192, 384, 48, 128, 128)

        self.avgpool = nn.AvgPool2d(8, stride=1)
        self.linear = nn.Linear(1024, 10)
        

    def forward(self, x):
        out = self.pre_layers(x)
        out = self.a3(out)
        out = self.b3(out)
        out = self.maxpool(out)
        out = self.a4(out)
        out = self.b4(out)
        out = self.c4(out)
        out = self.d4(out)
        out = self.e4(out)
        out = self.maxpool(out)
        out = self.a5(out)
        out = self.b5(out)
        out = self.avgpool(out)
        out = out.view(out.size(0), -1) # flatten
        out = self.linear(out)
        return out

# NETWORK =============================================================        
cnn = GoogLeNet(); # iniciliazece modelu
if cuda_available:
    cnn.cuda() # nastaveni pouziti cudy
    cnn = torch.nn.DataParallel(cnn, device_ids=range(torch.cuda.device_count()))

criterion = nn.CrossEntropyLoss() # 
optimizer = torch.optim.Adam(cnn.parameters(), lr=learning_rate) # optimalizer Adam

best_validation_accuracy = 0.0
last_improvement = 0

# Zastaveni pokud jiz dlouho nebyl lepsi vysledek
require_improvement = 10000
total_iterations = 0


def optimize(num_iterations,print_info):
    cnn.train(True)
    # přistup k globalnim promenym
    global total_iterations
    global best_validation_accuracy
    global last_improvement

    acc_arg = 0
    acc_val_arg = 0
    loss_arg = 0 
    time_arg = 0

    iter_num = 0

    start_time = time.time()
    for i in range(num_iterations):
        start_iter = time.time()
        train_iter = iter(train_loader) #pujde iterovat
        images, labels = train_iter.next() # dalsi trenovaci sada
        if cuda_available:
            images = Variable(images).cuda()
            labels = Variable(labels).cuda()
        else:
            images = Variable(images)
            labels = Variable(labels)
        total_iterations += 1

        # Forward + Backward + Optimize
        optimizer.zero_grad()
        outputs = cnn(images).cuda()
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        
        if print_info:
        
            if (total_iterations % 100 == 0):
                end_iter = time.time()
                diff_iter = end_iter - start_iter
                # Spocitani presnosti z cele validační sady
                acc_validation, acc = Test_accuracy()
                
                #  Validacni sada je lepsi jak nejlepší sada
                if acc_validation > best_validation_accuracy:

                    best_validation_accuracy = acc_validation
                    # Ulozeni posledni zmeny
                    last_improvement = i
                    # Ulozi session
                    torch.save(cnn.state_dict(), 'cnn_CIFAR.pkl')
                    improved_str = ' - Save'
                else:
                    improved_str = ''

                #print ('Iteration [%d],  Loss: %.4f Accureny: %.4f ' % (total_iterations, loss.data[0], acc*100)+improved_str)
        

                msg = "{0:>6}; {1:>6.1%}; {2}; {3}; {4} "
                print(msg.format(total_iterations, acc, diff_iter, loss.data[0], improved_str))
                iter_num += 1
                

                acc_val_arg += acc
                loss_arg += loss.data[0]
                time_arg += diff_iter

            # Dlouho neproběhlo lepsi nacitani
            if total_iterations - last_improvement > require_improvement:
                print("Stop Training")
                break
            

    end_time = time.time()
    time_dif = end_time - start_time
    # Cas zpracovani.
    print("Time usage: " + str(timedelta(seconds=int(round(time_dif)))))
    #print("Test info: {0} {1} {2} {3:>6.1%} {4:>6.1%}".format(time_arg/iter_num,loss_arg/iter_num,timedelta(seconds=int(round(time_dif))),acc_arg/iter_num,acc_val_arg/iter_num))


def Test_accuracy(): # presnost datasetu
    cnn.train(False) # nejedna se o trenink
    label_array = [] # pole pro napisy
    predicated_array = [] # pole na predpoved
    for images, labels in test_loader: # dalsi iterace 
        if cuda_available:
            images = Variable(images).cuda() # nastaveni obrazku jako promenne
        else:
            images = Variable(images)
        outputs = cnn(images) # vystup ze site
        _, predicted = torch.max(outputs.data, 1) # ze ktere je pravdepodobne tridy
        label_array = np.concatenate((label_array,labels),axis = 0) # spojeni pole
        predicated_array = np.concatenate((predicated_array,predicted),axis=0)
       
    correct = (predicated_array == label_array) # co je spravne a co ne
    cnn.train(True) # zapne trenovaci mod

    correct_sum = correct.sum() # vypocet kolik je spravne
    acc = float(correct_sum) / len(correct) # spravne deleno z kolika a vypoctu procentualni uspesnost
    return correct_sum,acc

def Train_accuracy(): # presnost train setu
    cnn.train(False) # nejedna se o trenink
    label_array = [] # pole pro popisky
    predicated_array = [] # pole pro predikci
    for images, labels in train_loader: # dalsi iterace z train loaderu
        if cuda_available:
            images = Variable(images).cuda() # nastaveni promenne
        else:
            images = Variable(images)
        
        outputs = cnn(images) # vystup z modelu
        _, predicted = torch.max(outputs.data, 1) # zarazeni do tridy
        label_array = np.concatenate((label_array,labels),axis = 0) # spojeni pole
        predicated_array = np.concatenate((predicated_array,predicted),axis=0) 
    
    correct = (predicated_array == label_array) # spravnost
    cnn.train(True) # zapnuti trenovani

    correct_sum = correct.sum() # secteni spravnych
    acc = float(correct_sum) / len(correct) # vypocet presnosti
    return correct_sum,acc

def plot_confusion_matrix(cls_pred,cls_true): # konvolucni matice
    
    cm = confusion_matrix(y_true=cls_true,
                          y_pred=cls_pred)
    cm_normalize = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
<<<<<<< HEAD:20Aplication/Pytorch/TorchGoogLeNetCIFAR.py
    print(cm_normalize) #vypis do konzole
=======
    print(cm) #vypis do konzole
>>>>>>> Testovaci:20 Aplication/Pytorch/TorchGoogLeNetCIFAR.py
    plt.matshow(cm_normalize) # do plotu
    plt.colorbar() # barvy
    tick_marks = np.arange(num_classes)
    plt.xticks(tick_marks, all_categories, rotation=90)
    plt.yticks(tick_marks, all_categories)
    plt.xlabel('Predicted')
    plt.ylabel('True')

    plt.show()

def plot_example_errors(cls_pred, correct): # zobrazeni spatne vyhodnocenych
    
    dataiter = iter(test_loaderAll)
    images, labels = dataiter.next()
    out = torchvision.utils.make_grid(images)
    out = images.numpy()
    out_labels = labels.numpy() # uprava na numpy

    incorrect = (correct == False) # spatne
    out = out[incorrect,:,:]
    cls_pred = cls_pred[incorrect] # 
    cls_true = out_labels[incorrect]
    
    plot_images(images=out[0:9],
                cls_true=cls_true[0:9],
                cls_pred=cls_pred[0:9]) # vykresleni


def print_test_accuracy(show_example_errors=False,
                        show_confusion_matrix=False): # vypis uspesnosti
    
    cnn.eval()  
    cnn.train(False) # nejedna se o trenink
    label_array = [] # pole pro popisky
    predicated_array = [] # pole pro predikci
    for images, labels in test_loader: # dalsi iterace z train loaderu
        #plot_images_numpy(images[0:9],labels[0:9])
        if cuda_available:
            images = Variable(images).cuda() # nastaveni promenne
        else:
            images = Variable(images)
        outputs = cnn(images) # vystup z modelu
        _, predicted = torch.max(outputs.data, 1) # zarazeni do tridy
        label_array = np.concatenate((label_array,labels),axis = 0) # spojeni pole
        predicated_array = np.concatenate((predicated_array,predicted),axis=0) 
    
    correct = (predicated_array == label_array)
    correct_sum = correct.sum()
    cnn.train(True)
    total = len(test_dataset.test_data)
    print('Accuracy on Test-Set: %f ( %d / %d ) ' % ((100 * (float)(correct_sum) / total),correct_sum,total))

    if show_example_errors:
        print("Example errors:")
        plot_example_errors(cls_pred=predicated_array, correct=correct)

    if show_confusion_matrix:
        print("Confusion Matrix:")
        plot_confusion_matrix(predicated_array,label_array)

def Restore_Session(): 
    cuda = torch.cuda.is_available()
    cnn.load_state_dict(torch.load('cnn_CIFAR.pkl'))

def main():
   #nacteni ulozene session
    #restoreSession()
    #vypis inforamci
    TestData()
    print(tf.VERSION
    #zapnuti optimalozeru)
    optimize(num_iterations=20000)
    #vypis
    print_test_accuracy(False,True)

if __name__ == "__main__":
    main()
