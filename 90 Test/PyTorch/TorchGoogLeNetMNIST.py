
from PIL import Image
import matplotlib.pyplot as plt
import torch 
import torch.nn as nn
import torchvision
import torchvision.datasets as dsets
import torchvision.transforms as transforms
from torch.autograd import Variable
from sklearn.metrics import confusion_matrix
from datetime import timedelta
import numpy as np
import time
import torch.multiprocessing as mp



transform_train = transforms.Compose([
    transforms.RandomCrop(28, padding=1),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
]) # normalizace,nastaveni transoformu pri trenovani

transform_test = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
]) # normalizece
#======================== MNIST Dataset==================================================================
train_dataset = dsets.MNIST(root='./data', train=True, download=True, transform=transform_train) # nacteni datasetu train

test_dataset = dsets.MNIST(root='./data', train=False, download=True, transform=transform_test) #nacteni datesetu test

# Parametry
num_epochs = 5
batch_size = 10
learning_rate = 0.001
img_size = 28
img_shape = (img_size, img_size) #image shape
num_classes = 10;

# Data Loader
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size, 
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=batch_size,
                                          shuffle=False)

#=====================================================Plot=================================================#
def plot_images_numpy(images,cls_true,cls_pred=None): # vykresleni obrazku,konvertace na numpy pole
    out = torchvision.utils.make_grid(images)
    out = images.numpy()
    plot_images(out,cls_true,cls_pred)

def plot_images(images, cls_true, cls_pred=None): # funkce na vykresleni
    assert len(images) == len(cls_true) == 9

    fig, axes = plt.subplots(3, 3) # subploty
    fig.subplots_adjust(hspace=0.3, wspace=0.3) # padding mezi subploty

    for i, ax in enumerate(axes.flat):
       
        ax.imshow(images[i].reshape(img_shape), cmap='binary') # vykresleni
        if cls_pred is None:
            xlabel = "True: {0}".format(cls_true[i])
        else:
            xlabel = "True: {0}, Pred: {1}".format(cls_true[i], cls_pred[i])


        ax.set_xlabel(xlabel) # nastaveni osy x

        ax.set_xticks([])
        ax.set_yticks([])

    plt.show()

def plot_image_numpy(image): # vykresleni jednoho obrazku a konvertace na numpy
    out = torchvision.utils.make_grid(image)
    out = image.numpy()
    plot_image(out)

def plot_image(image): 
    plt.imshow(image.reshape(img_shape),
               interpolation='nearest',
               cmap='binary')

    plt.show()

def Dataset_Info(): # info o datasetu
    print("Size of:")
    print("- Training-set:\t\t{}".format(len(train_dataset)))
    print("- Test-set:\t\t{}".format(len(test_dataset)))
    
# GoogLeNet Model =============================================================================

class Inception(nn.Module):
    def __init__(self, in_planes, n1x1, n3x3red, n3x3, n5x5red, n5x5, pool_planes):
        super(Inception, self).__init__()
        # 1x1 
        self.b1 = nn.Sequential(
            nn.Conv2d(in_planes, n1x1, kernel_size=1),
            nn.BatchNorm2d(n1x1),
            nn.ReLU(True),
        )

        # 1x1 -> 3x3 
        self.b2 = nn.Sequential(
            nn.Conv2d(in_planes, n3x3red, kernel_size=1),
            nn.BatchNorm2d(n3x3red),
            nn.ReLU(True),
            nn.Conv2d(n3x3red, n3x3, kernel_size=3, padding=1),
            nn.BatchNorm2d(n3x3),
            nn.ReLU(True),
        )

        # 1x1  -> 5x5 
        self.b3 = nn.Sequential(
            nn.Conv2d(in_planes, n5x5red, kernel_size=1),
            nn.BatchNorm2d(n5x5red),
            nn.ReLU(True),
            nn.Conv2d(n5x5red, n5x5, kernel_size=3, padding=1),
            nn.BatchNorm2d(n5x5),
            nn.ReLU(True),
            nn.Conv2d(n5x5, n5x5, kernel_size=3, padding=1),
            nn.BatchNorm2d(n5x5),
            nn.ReLU(True),
        )

        # 3x3 pool -> 1x1 conv 
        self.b4 = nn.Sequential(
            nn.MaxPool2d(3, stride=1, padding=1),
            nn.Conv2d(in_planes, pool_planes, kernel_size=1),
            nn.BatchNorm2d(pool_planes),
            nn.ReLU(True),
        )

    def forward(self, x):
        y1 = self.b1(x)
        y2 = self.b2(x)
        y3 = self.b3(x)
        y4 = self.b4(x)
        return torch.cat([y1,y2,y3,y4], 1)

class GoogLeNet(nn.Module):
    def __init__(self):
        super(GoogLeNet, self).__init__()
        self.pre_layers = nn.Sequential(
            nn.Conv2d(1, 192, kernel_size=3, padding=1),
            nn.BatchNorm2d(192),
            nn.ReLU(True),
        )

        self.a3 = Inception(192,  64,  96, 128, 16, 32, 32)
        self.b3 = Inception(256, 128, 128, 192, 32, 96, 64)

        self.maxpool = nn.MaxPool2d(3, stride=2, padding=1)

        self.a4 = Inception(480, 192,  96, 208, 16,  48,  64)
        self.b4 = Inception(512, 160, 112, 224, 24,  64,  64)
        self.c4 = Inception(512, 128, 128, 256, 24,  64,  64)
        self.d4 = Inception(512, 112, 144, 288, 32,  64,  64)
        self.e4 = Inception(528, 256, 160, 320, 32, 128, 128)

        self.a5 = Inception(832, 256, 160, 320, 32, 128, 128)
        self.b5 = Inception(832, 384, 192, 384, 48, 128, 128)

        self.avgpool = nn.AvgPool2d(7, stride=1)
        self.linear = nn.Linear(1024, 10)

    def forward(self, x):
        out = self.pre_layers(x)
        out = self.a3(out)
        out = self.b3(out)
        out = self.maxpool(out)
        out = self.a4(out)
        out = self.b4(out)
        out = self.c4(out)
        out = self.d4(out)
        out = self.e4(out)
        out = self.maxpool(out)
        out = self.a5(out)
        out = self.b5(out)
        out = self.avgpool(out)
        out = out.view(out.size(0), -1) # flatten
        out = self.linear(out)
        return out


# NETWORK =============================================================        
cnn = GoogLeNet(); # iniciliazece modelu
cnn.cuda() # nastaveni pouziti cudy
cnn.share_memory()

criterion = nn.CrossEntropyLoss() # 
optimizer = torch.optim.Adam(cnn.parameters(), lr=learning_rate) # optimalizer Adam


best_validation_accuracy = 0.0
last_improvement = 0

# Zastaveni pokud jiz dlouho nebyl lepsi vysledek
require_improvement = 3000
total_iterations = 0

def optimize(num_iterations):
    cnn.train(True)
    # přistup k globalnim promenym
    global total_iterations
    global best_validation_accuracy
    global last_improvement

    start_time = time.time()
    for i in range(num_iterations):
        train_iter = iter(train_loader) #pujde iterovat
        images, labels = train_iter.next() # dalsi trenovaci sada
        images = Variable(images).cuda()
        labels = Variable(labels).cuda()
        total_iterations +=1

        # Forward + Backward + Optimize
        optimizer.zero_grad()
        outputs = cnn(images)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
            

        if (total_iterations % 100 == 0):
            # Spocitani presnosti z cele validační sady
            acc_validation, acc = Test_accuracy()
            #  Validacni sada je lepsi jak nejlepší sada 
            if acc_validation > best_validation_accuracy:
                    
                best_validation_accuracy = acc_validation         
                # Ulozeni posledni zmeny
                last_improvement = i
                # Ulozi session
                torch.save(cnn.state_dict(), 'cnn_MNIST.pkl')
                improved_str = ' - Save'
            else:
                improved_str = ''
                
            print ('Iteration [%d],  Loss: %.4f Accureny: %.4f '%( total_iterations, loss.data[0],acc*100)+improved_str)
            
        # Dlouho neproběhlo lepsi nacitani      
        if total_iterations - last_improvement > require_improvement:
            print("Stop Training")
            break
        
    end_time = time.time()
    time_dif = end_time - start_time
    
    # Cas zpracovani.
    print("Time usage: " + str(timedelta(seconds=int(round(time_dif)))))

def Test_accuracy(): # presnost datasetu
    cnn.eval()  # nastaveni eval
    cnn.train(False) # nejedna se o trenink
    label_array = [] # pole pro napisy
    predicated_array = [] # pole na predpoved
    for images, labels in test_loader: # dalsi iterace 
        images = Variable(images).cuda() # nastaveni obrazku jako promenne
        outputs = cnn(images) # vystup ze site
        _, predicted = torch.max(outputs.data, 1) # ze ktere je pravdepodobne tridy
        label_array = np.concatenate((label_array,labels),axis = 0) # spojeni pole
        predicated_array = np.concatenate((predicated_array,predicted),axis=0)
       
    correct = (predicated_array == label_array) # co je spravne a co ne
    cnn.train(True) # zapne trenovaci mod

    correct_sum = correct.sum() # vypocet kolik je spravne
    acc = float(correct_sum) / len(correct) # spravne deleno z kolika a vypoctu procentualni uspesnost
    return correct_sum,acc

def Train_accuracy(): # presnost train setu
    cnn.eval()  
    cnn.train(False) # nejedna se o trenink
    label_array = [] # pole pro popisky
    predicated_array = [] # pole pro predikci
    for images, labels in train_loader: # dalsi iterace z train loaderu
        images = Variable(images).cuda() # nastaveni promenne
        outputs = cnn(images) # vystup z modelu
        _, predicted = torch.max(outputs.data, 1) # zarazeni do tridy
        label_array = np.concatenate((label_array,labels),axis = 0) # spojeni pole
        predicated_array = np.concatenate((predicated_array,predicted),axis=0) 
    
    correct = (predicated_array == label_array) # spravnost
    cnn.train(True) # zapnuti trenovani

    correct_sum = correct.sum() # secteni spravnych
    acc = float(correct_sum) / len(correct) # vypocet presnosti
    return correct_sum,acc

def plot_confusion_matrix(cls_pred,cls_true): # konvolucni matice
    
    cm = confusion_matrix(y_true=cls_true,
                          y_pred=cls_pred)
    print(cm) #vypis do konzole
    plt.matshow(cm) # do plotu
    plt.colorbar() # barvy
    tick_marks = np.arange(num_classes)
    plt.xticks(tick_marks, range(num_classes))
    plt.yticks(tick_marks, range(num_classes))
    plt.xlabel('Predicted')
    plt.ylabel('True')

    plt.show()

def plot_example_errors(cls_pred, correct): # zobrazeni spatne vyhodnocenych
    
    out = torchvision.utils.make_grid(test_dataset.test_data) # uprava pole
    out = out.numpy() # uprava do numpy
    out_labels = test_dataset.test_labels.numpy() # uprava na numpy

    incorrect = (correct == False) # spatne
    images = out[incorrect] # spatne obrazky
    cls_pred = cls_pred[incorrect] # 
    cls_true = out_labels[incorrect]
    
    plot_images(images=images[0:9],
                cls_true=cls_true[0:9],
                cls_pred=cls_pred[0:9]) # vykresleni


def print_test_accuracy(show_example_errors=False,
                        show_confusion_matrix=False): # vypis uspesnosti

    cnn.eval()  
    cnn.train(False) # nejedna se o trenink
    label_array = [] # pole pro popisky
    predicated_array = [] # pole pro predikci
    for images, labels in test_loader: # dalsi iterace z train loaderu
        images = Variable(images).cuda() # nastaveni promenne
        outputs = cnn(images) # vystup z modelu
        _, predicted = torch.max(outputs.data, 1) # zarazeni do tridy
        label_array = np.concatenate((label_array,labels),axis = 0) # spojeni pole
        predicated_array = np.concatenate((predicated_array,predicted),axis=0) 
        
    total = len(test_dataset.test_data)
    correct = (predicated_array == label_array)
    correct_sum = correct.sum()
    cnn.train(True)

    print('Accuracy on Test-Set: %f ( %d / %d ) ' % ((100 * (float)(correct_sum) / total),correct_sum,total))

    if show_example_errors:
        print("Example errors:")
        plot_example_errors(cls_pred=predicated_array, correct=correct)

    if show_confusion_matrix:
        print("Confusion Matrix:")
        plot_confusion_matrix(predicated_array,label_array)

def Restore_Session(): 
    cuda = torch.cuda.is_available()
    cnn.load_state_dict(torch.load('cnn_MNIST.pkl'))

def main():
    cuda = torch.cuda.is_available()
    print torch.backends.cudnn.version()
    Dataset_Info()
    optimize(1000)
    #Restore_Session()
    print_test_accuracy(True,True)


if __name__ == "__main__":
    main()
