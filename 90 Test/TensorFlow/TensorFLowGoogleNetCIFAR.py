import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from sklearn.metrics import confusion_matrix
import math
import time
from datetime import timedelta
import os
import cifar10

cifar10.maybe_download_and_extract() #stahne dataset CIFAR10
class_names = cifar10.load_class_names() #pole nazvu trid 
images_train, cls_train, labels_train = cifar10.load_training_data() # set trenovacich obrazku a jejich nazvu
images_test, cls_test, labels_test = cifar10.load_test_data() # set testovacich obrazku a jejich nazev
FLAGS = tf.app.flags.FLAGS
all_categories = []


tf.app.flags.DEFINE_integer('num_gpus', 4,
                            """How many GPUs to use.""")


for x in range(0,10):
    all_categories.append(class_names[x])

def TestData(): # funkce vypisujici inforamce datasetu
    print("Size of:")
    print("- Training-set:\t\t{}".format(len(images_train)))
    print("- Test-set:\t\t{}".format(len(images_test)))

from cifar10 import img_size, num_channels, num_classes 
img_shape = (img_size, img_size) # dimenze obrazku -> x,y
img_size_flat = img_size * img_size # obrazkek v jedne dimenzi 
isTraining = False; # trenuju ? ANO, NE

def plot_images(images, cls_true, cls_pred=None, smooth=True): # vykresleni obrazku
    assert len(images) == len(cls_true) == 9 # v jednom plotu bude 9 obrazku
    fig, axes = plt.subplots(3, 3) # matice 3x3

    if cls_pred is None:
        hspace = 0.3 # mezera mezi subploty
    else:
        hspace = 0.6
    fig.subplots_adjust(hspace=hspace, wspace=0.3) # nastaveni mezer sublotu

    for i, ax in enumerate(axes.flat):
        if smooth: # pouziti smooth
            interpolation = 'spline16'
        else:
            interpolation = 'nearest'

        ax.imshow(images[i, :, :, :],interpolation=interpolation) # vykresleni
        numberClass = cls_true[i] # vypsani do jake tridy patri dle predikce
        cls_true_name = class_names[numberClass] # do jake tridy patri dle datasetu

        if cls_pred is None: # vypisuji bez predikce
            xlabel = "True: {0}".format(cls_true_name)
        else: # s predikci
            cls_pred_name = class_names[cls_pred[i]]
            xlabel = "True: {0}\nPred: {1}".format(cls_true_name, cls_pred_name)

        ax.set_xlabel(xlabel) # nastaveni popisku      
        ax.set_xticks([]) # souradnice x nebudou
        ax.set_yticks([]) # souradnice y nebudou

    plt.show()


#Nastaveni TF variables

x = tf.placeholder(tf.float32, shape=[None, img_size, img_size, num_channels], name='x') #placeholder promenna obsahujici obrazky
y_true = tf.placeholder(tf.float32, shape=[None, num_classes], name='y_true') # placeholder pormenna obsahujici pravdive promenn0
y_true_cls = tf.argmax(y_true, dimension=1) # obsahjuje cislo tridy.

#Google Net
def inception_layer(inputs,
                    conv_11_size,
                    conv_33_reduce_size, conv_33_size,
                    conv_55_reduce_size, conv_55_size,
                    pool_size,
                    name='inception'):

    # Inception modelu konvolucni site
    conv_11 = tf.layers.conv2d(inputs, filters = conv_11_size, kernel_size = [1,1],padding="same", activation=tf.nn.relu, name='{}_1x1'.format(name))
    conv_33_reduce = tf.layers.conv2d(inputs, filters =conv_33_reduce_size,kernel_size = [1,1],padding="same", activation=tf.nn.relu, name='{}_3x3_reduce'.format(name))
    conv_33 = tf.layers.conv2d(conv_33_reduce,filters = conv_33_size,kernel_size =[3,3],padding="same",activation=tf.nn.relu, name = '{}_3x3'.format(name))
    conv_55_reduce = tf.layers.conv2d(inputs,filters = conv_55_reduce_size, kernel_size=[1,1],padding="same",activation=tf.nn.relu, name = '{}_5x5_reduce'.format(name) )
    conv_55  = tf.layers.conv2d(conv_55_reduce, filters = conv_55_size, kernel_size = [5,5],padding="same", activation=tf.nn.relu, name= '{}_5x5'.format(name))
    pool = tf.layers.max_pooling2d(inputs, pool_size=[3,3],strides=1,padding="same",name= '{}_5x5'.format(name) )
    convpool = tf.layers.conv2d(pool, filters = pool_size, kernel_size = [1,1], activation=tf.nn.relu, name='{}_pool_proj'.format(name),padding="same")
    
    return tf.concat([conv_11, conv_33, conv_55, convpool],3, name='{}_concat'.format(name)) # pojeni jednotlivych vrstev

def GoogLeNet(inputs): #navrh GoogLeNet modelu
    conv1_7_7 = tf.layers.conv2d(inputs, filters = 64, kernel_size = [7,7] , strides=2, activation=tf.nn.relu, name = 'conv1_7_7_s2',padding="same")
    pool1_3_3 = tf.layers.max_pooling2d(conv1_7_7, pool_size = [3,3],strides=2,padding="same")
    pool1_3_3 = tf.nn.local_response_normalization(pool1_3_3)
    conv2_3_3_reduce = tf.layers.conv2d(pool1_3_3, filters = 64,kernel_size = [1,1], activation=tf.nn.relu,name = 'conv2_3_3_reduce',padding="same")
    conv2_3_3 = tf.layers.conv2d(conv2_3_3_reduce, filters = 192,kernel_size=[3,3], activation=tf.nn.relu, name='conv2_3_3',padding="same")
    conv2_3_3 = tf.nn.local_response_normalization(conv2_3_3)
    pool2_3_3 = tf.layers.max_pooling2d(conv2_3_3,pool_size=[3,3], strides=2, name='pool2_3_3_s2',padding="same")

    a3 = inception_layer(pool2_3_3,  64,  96, 128, 16, 32, 32,'inception_a3')
    b3 = inception_layer(a3, 128, 128, 192, 32, 96, 64,'inception_b3')

    maxpool = tf.layers.max_pooling2d(b3, pool_size=3, strides=2, name='pool_3_3',padding="same")

    a4 = inception_layer(maxpool, 192,  96, 208, 16,  48,  64,'inception_a4')
    b4 = inception_layer(a4, 160, 112, 224, 24,  64,  64,'inception_b4')
    c4 = inception_layer(b4, 128, 128, 256, 24,  64,  64,'inception_c4')
    d4 = inception_layer(c4, 112, 144, 288, 32,  64,  64,'inception_d4')
    e4 = inception_layer(d4, 256, 160, 320, 32, 128, 128,'inception_e4')

    a5 = inception_layer(e4, 256, 160, 320, 32, 128, 128,'inception_a5')
    b5 = inception_layer(a5, 384, 192, 384, 48, 128, 128,'inception_b5')

    pool_7_7 = tf.layers.average_pooling2d(b5, pool_size=[7,7], strides=1,padding="same") # prumerovy pooling

    flat = tf.contrib.layers.flatten(pool_7_7) # spolostovaci vrstva
    dense = tf.layers.dense(inputs=flat, name='layer_dense',units=1024, activation=tf.nn.relu) # spojovaci vrstva s 1024 neurony
    dense = tf.layers.dense(inputs=dense, name='layer_fc_out',units=num_classes, activation=None) # vrstva s rozdelenim do trid
    
    return dense;
    
logits = GoogLeNet(x) # volani modulu
with tf.variable_scope(tf.get_variable_scope()):
    for i in xrange(FLAGS.num_gpus): # rozdeleni prace do vice GPU
        with tf.device('/gpu:%d' % i):

            
            y_pred = tf.nn.softmax(logits=logits) #sofmax pro zjisteni pravdepodopnosti
            y_pred_cls = tf.argmax(y_pred, dimension=1) # rozdeleni do tridy

            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=y_true, logits=logits)

            loss = tf.reduce_mean(cross_entropy) # vypocet ztraty
            
            optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(loss) # optimalizace vah
            correct_prediction = tf.equal(y_pred_cls, y_true_cls) # porovnani predikce a pravdy
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32)) 


saver = tf.train.Saver() # ukladani
save_dir = 'save/' # slozka k ulozeni
if not os.path.exists(save_dir):
    os.makedirs(save_dir) # pokud neexstuje slozka tak vytvori
save_path = os.path.join(save_dir, 'best_validation_CIFAR10000') # cesta k ulozeni

config = tf.ConfigProto()
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer()) #inicialzece promennych

train_batch_size = 64 #pocet obrazku v sade
best_validation_accuracy = 0.0 #nejlepsi vysledek
last_improvement = 0 # posledni provedena zmena

# Zastaveni pokud jiz dlouho nebyl lepsi vysledek
require_improvement = 3000
total_iterations = 0

train_interation = 0
def random_batch(): # nahodne obrazky z datasetu train
    global train_interation
    num_images = len(images_train)
    idx = np.random.choice(num_images,size=train_batch_size,replace=False)

    x_batch = images_train[idx, :, :, :]
    y_batch = labels_train[idx, :]

    return x_batch, y_batch

def optimize(num_iterations): # trenovaci funkce
    # přistup k globalnim promenym
    global total_iterations
    global best_validation_accuracy
    global last_improvement
    isTraining = True;
    
    start_time = time.time()
    for i in range(num_iterations):
        total_iterations += 1
        # Nacteni dat a label
        x_batch, y_true_batch = random_batch()

        # ulozeni do feed_dict
        feed_dict_train = {x: x_batch,y_true: y_true_batch}

        #Spusteni optimalizeru 
        session.run(optimizer, feed_dict=feed_dict_train)
        # Vytisteni informaci po kazdých 100 iteracích
        if (total_iterations % 100 == 0) or (i == (num_iterations - 1)):
            # Spocitani presnosti z train baličku sady
            acc_train = session.run(accuracy, feed_dict=feed_dict_train) 
            # Spocitani presnosti z cele validační sady
            acc_validation, _ = validation_accuracy()
            #  Validacni sada je lepsi jak nejlepší sada 
            if acc_validation > best_validation_accuracy:
                
                best_validation_accuracy = acc_validation         
                # Ulozeni posledni zmeny
                last_improvement = total_iterations
                # Ulozi session
                saver.save(sess=session, save_path=save_path)
                improved_str = ' - Save'
            else:
                improved_str = ''
            
            # Stav
            msg = "Iterace: {0:>6}, Train-Batch Accuracy: {1:>6.1%}, Validation Accuracy: {2:>6.1%} {3}"
            # Tisk + tisk do log
            print(msg.format(i + 1, acc_train, acc_validation, improved_str))
            
        # Dlouho neproběhlo lepsi nacitani
        if total_iterations - last_improvement > require_improvement:
            print("Stop Training")
            break

    end_time = time.time()
    time_dif = end_time - start_time
    # Cas zpracovani.
    print("Trainig Time: " + str(timedelta(seconds=int(round(time_dif)))))
    isTraining = False;

def optimizeInfinity(): # nekonecne trenovani
    # přistup k globalnim promenym
    global total_iterations
    global best_validation_accuracy
    global last_improvement
    isTraining = True;
    
    start_time = time.time()
    best_validation_accuracy, _ = validation_accuracy()
    while(True):
        total_iterations += 1
        # Nacteni dat a label
        x_batch, y_true_batch = random_batch()

        # ulozeni do feed_dict
        feed_dict_train = {x: x_batch,y_true: y_true_batch}

        #Spusteni optimalizeru 
        session.run(optimizer, feed_dict=feed_dict_train)
        # Vytisteni informaci po kazdých 100 iteracích
        if (total_iterations % 100 == 0):
            # Spocitani presnosti z train baličku sady
            acc_train = session.run(accuracy, feed_dict=feed_dict_train) 
            # Spocitani presnosti z cele validační sady
            acc_validation, _ = validation_accuracy()
            #  Validacni sada je lepsi jak nejlepší sada 
            if acc_validation > best_validation_accuracy:
                
                best_validation_accuracy = acc_validation         
                # Ulozeni posledni zmeny
                last_improvement = total_iterations
                # Ulozi session
                saver.save(sess=session, save_path=save_path)
                improved_str = ' - Save'
            else:
                improved_str = ''
            # Stav
            msg = "Iterace: {0:>6}, Train-Batch Accuracy: {1:>6.1%}, Validation Accuracy: {2:>6.1%} {3}"
            # Tisk + tisk do log
            print(msg.format(total_iterations, acc_train, acc_validation, improved_str))

    end_time = time.time()
    time_dif = end_time - start_time
    # Cas zpracovani.
    print("Trainig Time: " + str(timedelta(seconds=int(round(time_dif)))))
    isTraining = False;


#validacni 
def predict_cls_validation():
    return predict_cls(images = images_test,
                       labels = labels_test,
                       cls_true = cls_test)

#spocita spravnost
def cls_accuracy(correct):  
    correct_sum = correct.sum()
    acc = float(correct_sum) / len(correct)

    return acc, correct_sum

def validation_accuracy():
    correct, _ = predict_cls_validation()
    return cls_accuracy(correct)

def restoreSession():
    saver.restore(sess=session, save_path=save_path)


def plot_example_errors(cls_pred, correct): # zobrazeni erroru
    incorrect = (correct == False)
    images = images_test[incorrect]
    cls_pred = cls_pred[incorrect]
    cls_true = cls_test[incorrect] 
    plot_images(images=images[0:9],cls_true=cls_true[0:9],cls_pred=cls_pred[0:9])

def plot_confusion_matrix(cls_pred): #zobrazeni pravdivostni matice
    cls_true = cls_test # pravdivé tridy
    cm = confusion_matrix(y_true=cls_true,y_pred=cls_pred) #funkce na vytvoreni matice

    print(cm) # vypsání matice

    plt.matshow(cm) # matice do grafu
    plt.colorbar()
    tick_marks = np.arange(num_classes)
    plt.xticks(tick_marks,  all_categories, rotation=90) #osa X
    plt.yticks(tick_marks,  all_categories)# osa Y
    plt.xlabel('Predicted') # popisky X
    plt.ylabel('True') # popisky Y

    plt.show()

test_batch_size = 256

def print_test_accuracy(show_example_errors=False,
                        show_confusion_matrix=False): # vypocet procentualni spravnosti

    num_test = len(images_test)
    cls_pred = np.zeros(shape=num_test, dtype=np.int)
    i = 0

    while i < num_test:
        j = min(i + test_batch_size, num_test)
        images = images_test[i:j, :, :, :]
        labels = labels_test[i:j, :]
        feed_dict = {x: images,
                     y_true: labels}

        cls_pred[i:j] = session.run(y_pred_cls, feed_dict=feed_dict)

        i = j

    cls_true = cls_test
    correct = (cls_true == cls_pred)
    correct_sum = correct.sum()
    acc = float(correct_sum) / num_test
    msg = "Accuracy on Test-Set: {0:.1%} ({1} / {2})"
    print(msg.format(acc, correct_sum, num_test))

    if show_example_errors:
        print("Example errors:")
        plot_example_errors(cls_pred=cls_pred, correct=correct)

    if show_confusion_matrix:
        print("Confusion Matrix:")
        plot_confusion_matrix(cls_pred=cls_pred)

batch_size = 256

def predict_cls(images, labels, cls_true): #predpoved konktretnich obrazku
    num_images = len(images)
    cls_pred = np.zeros(shape=num_images, dtype=np.int)
    i = 0

    while i < num_images:
        j = min(i + batch_size, num_images)
        feed_dict = {x: images[i:j, :, :, :], y_true: labels[i:j, :]}
        cls_pred[i:j] = session.run(y_pred_cls, feed_dict=feed_dict)
        i = j

    correct = (cls_true == cls_pred)

    return correct, cls_pred

def plot_image(image): # vykresleni jednoho konkretniho obrazku
    
    fig, axes = plt.subplots(1, 2)

    ax0 = axes.flat[0]
    ax1 = axes.flat[1]

    ax0.imshow(image, interpolation='nearest')
    ax1.imshow(image, interpolation='spline16')

    ax0.set_xlabel('Raw')
    ax1.set_xlabel('Smooth')
    
    plt.show()

def main():
    #restoreSession()
    TestData()
    print(tf.VERSION)

    #optimizeInfinity()
    ##print_test_accuracy(True,True)
    #optimize(num_iterations=10000)
    #print_test_accuracy(True,True)
    #image1 = images_test[0]
    #plot_image(image1)

if __name__ == "__main__":
    main()
